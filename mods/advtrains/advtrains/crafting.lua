--advtrains by orwell96, see readme.txt and license.txt
--crafting.lua
--registers crafting recipes

--tracks: see advtrains_train_track
--signals
minetest.register_craft({
	output = 'advtrains:retrosignal_off 2',
	recipe = {
		{'mcl_dyes:red', 'moreores:tin_ingot', 'moreores:tin_ingot'},
		{'', '', 'moreores:tin_ingot'},
		{'', '', 'moreores:tin_ingot'},
	},
})
minetest.register_craft({
	output = 'advtrains:signal_off 2',
	recipe = {
		{'', 'mcl_dyes:red', 'moreores:tin_ingot'},
		{'', 'mcl_dyes:dark_green', 'moreores:tin_ingot'},
		{'', '', 'moreores:tin_ingot'},
	},
})
--Wallmounted Signal
minetest.register_craft({
	output = 'advtrains:signal_wall_r_off 2',
	recipe = {
		{'mcl_dyes:red', 'moreores:tin_ingot', 'moreores:tin_ingot'},
		{'', 'moreores:tin_ingot', ''},
		{'mcl_dyes:dark_green', 'moreores:tin_ingot', 'moreores:tin_ingot'},
	},
})


--Wallmounted Signals can be converted into every orientation by shapeless crafting
minetest.register_craft({
	output = 'advtrains:signal_wall_l_off',
	type = "shapeless",
	recipe = {'advtrains:signal_wall_r_off'},
})
minetest.register_craft({
	output = 'advtrains:signal_wall_t_off',
	type = "shapeless",
	recipe = {'advtrains:signal_wall_l_off'},
})
minetest.register_craft({
	output = 'advtrains:signal_wall_r_off',
	type = "shapeless",
	recipe = {'advtrains:signal_wall_t_off'},
})

--trackworker
minetest.register_craft({
	output = 'advtrains:trackworker',
	recipe = {
		{'mcl_core:diamond'},
		{'screwdriver:screwdriver'},
		{'moreores:tin_ingot'},
	},
})

--boiler
minetest.register_craft({
	output = 'advtrains:boiler',
	recipe = {
		{'moreores:tin_ingot', 'moreores:tin_ingot', 'moreores:tin_ingot'},
		{'mcl_doors:iron_trapdoor', '', 'moreores:tin_ingot'},
		{'moreores:tin_ingot', 'moreores:tin_ingot', 'moreores:tin_ingot'},
	},
})

--drivers'cab
minetest.register_craft({
	output = 'advtrains:driver_cab',
	recipe = {
		{'moreores:tin_ingot', 'moreores:tin_ingot', 'moreores:tin_ingot'},
		{'', '', 'mcl_core:glass'},
		{'moreores:tin_ingot', 'moreores:tin_ingot', 'moreores:tin_ingot'},
	},
})

--drivers'cab
minetest.register_craft({
	output = 'advtrains:wheel',
	recipe = {
		{'', 'moreores:tin_ingot', ''},
		{'moreores:tin_ingot', 'group:stick', 'moreores:tin_ingot'},
		{'', 'moreores:tin_ingot', ''},
	},
})

--chimney
minetest.register_craft({
	output = 'advtrains:chimney',
	recipe = {
		{'', 'moreores:tin_ingot', ''},
		{'', 'moreores:tin_ingot', 'mcl_core:torch'},
		{'', 'moreores:tin_ingot', ''},
	},
})


--misc_nodes
--crafts for platforms see misc_nodes.lua
