# VoxeLibre TNT Sticks `vl_tnt_sticks` [WIP]

This mod adds throwable TNT sticks to VoxeLibre.

**Author:** Mikita 'rudzik8' Wiśniewski


## Types

There are 3 types of TNT sticks.

Regular ones will explode when touching ground.

Sticky ones will stick to surfaces and explode after 4 seconds. They are crafted
with a slime ball.

Fire ones will (similarly to regular ones) explode when touching ground, but
will also set things around on fire. Crafted with a fire charge.


## Crafting

![Regular TNT Stick recipe](https://i.imgur.com/qIOvlMQ.png)

![Sticky TNT Stick recipe](https://i.imgur.com/IjfmVjd.png)

![Fire TNT Stick recipe](https://i.imgur.com/pMJM3v2.png)


## License

The license of the source code is MIT. See [`LICENSE`](LICENSE) file for details.

The license of the media assets is CC BY-SA 4.0. See [`MEDIA_LICENSE`](MEDIA_LICENSE) file or [this deed](https://creativecommons.org/licenses/by-sa/4.0/) for details.
