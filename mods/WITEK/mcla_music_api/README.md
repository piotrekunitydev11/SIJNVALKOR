# mcla_music_api

A music API to register a soundtrack for your game. Fully compatible with registrations for the old "music_api" mod as seen in "music modpack".

Despite the name does not actually depend on mineclonia even though it will display a nice track selection menu in the mineclonia survival inventory (also with sfinv).

## Chatcommands
* /music next - play the next queueable so
* /music list - list all available tracks
* /music play tracknumber - play track from list
* /music 0-100 - set volume

## API
`mcla_music_api.register_on_play(function(player, song_definition) end)` - to register a function that will be executed when a song starts
`mcla_music_api.register_on_playback_complete(function(player, song_definition) end)` - to register a function that will be executed when a songs playback is complete.

`mcla_music_api.register_song(song_definition)`
### Song definition
These are 100% compatible with registration definitions for music_api. It even supports the music.register_track command which can be used interchangably.

mcla_music_api comes with a "generate_registrations.sh" script to attempt to automatically generate registrations from *.ogg files in a directory using vorbiscomment and ffmpeg, this is particularly helpful to determine the length of the songs.

However it supports a number of new fields not previously known to music_api:
```lua
{
	name = "soundfile",
	--"soundname" according to minetest i.e. the filename without the .ogg extension

	title = "Fancy Title",
	-- Optional: Display title, if not found `name` will be used

	length = 210,
	-- The length of the song in seconds, this is absolutely required since minetest has no way of telling when a sound playback is complete.

	day = true,
	-- Optional: if false song will not be played during daytime

	night = true,
	-- Optional: if false song will not be played during nighttime

	ymin = -32000,
	-- Optional: minimum y position for the song to be automatically queued

	ymax = 32000,
	-- Optional: maximum y position for the song to be automatically queued

	biomes = { "ColdTaiga" },
	-- Optional: List of biome names the song can be automatically queued in

	hidden = false,
	-- Optional: If true the song will not be shown in the tracklist

	check_player = function(player) end,
	-- Optional: if this function returns false the song will not be queued automatically
}
```
