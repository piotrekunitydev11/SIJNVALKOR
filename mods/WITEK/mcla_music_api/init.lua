mcla_music_api = {}
mcla_music_api.registered_songs = {}
mcla_music_api.registered_on_play = {}
mcla_music_api.registered_on_song_complete = {}
local time_between_min = tonumber(minetest.settings:get("music_time_interval_min")) or 60
local time_between_max = tonumber(minetest.settings:get("music_time_interval_max")) or 240

local now_playing = {}

function mcla_music_api.register_song(def)
	def.trackno = #mcla_music_api.registered_songs + 1
	table.insert(mcla_music_api.registered_songs, def)
end
function mcla_music_api.register_on_play(func)
	table.insert(mcla_music_api.registered_on_play, func)
end
function mcla_music_api.register_on_song_complete(func)
	table.insert(mcla_music_api.registered_on_song_complete, func)
end

-- compat with "music_api"
if not minetest.global_exists("music") then
	music = {}
	music.register_track = mcla_music_api.register_song
end

local function is_song_valid(player, def)
	local pos = player:get_pos()
	local tod = minetest.get_timeofday()
	local biome
	local bd = minetest.get_biome_data(pos)
	if bd then
		biome = minetest.get_biome_name(bd.biome)
	end
	if def.ymin and pos.y < def.ymin then return false end
	if def.ymax and pos.y > def.ymax then return false end
	if biome and def.biomes and table.indexof(def.biomes, biome) == -1 then return false end
	if def.day ~= nil and def.day and (tod < .25 or tod > .75) then return false end
	if def.night ~= nil and def.night and (tod > .25 or tod < .75) then return false end
	if def.check_player and def.check_player(player) == false then return false end
	return true
end

function mcla_music_api.pick_song(player)
	table.shuffle(mcla_music_api.registered_songs)
	for _,def in pairs(mcla_music_api.registered_songs) do
		if is_song_valid(player, def) then
			return def
		end
	end
end

function mcla_music_api.stop_playback(player)
	if now_playing[player] and now_playing[player].handle then
		minetest.sound_stop(now_playing[player].handle)
		now_playing[player].handle = nil
	end
end

function mcla_music_api.get_gain(player)
	local m = player:get_meta()
	local v = 100
	if m:get("mcla_music_api:volume") then
		v = m:get_int("mcla_music_api:volume")
	end
	return v / 100
end

function mcla_music_api.set_volume(player, volume)
	local m = player:get_meta()
	m:set_int("mcla_music_api:volume", math.ceil(volume))
	if now_playing[player].handle then
		minetest.sound_fade(now_playing[player].handle, 25, mcla_music_api.get_gain(player))
	end
end

function mcla_music_api.play_song(player, def)
	if not def then return end
	if now_playing[player] and now_playing[player].job_next then
		now_playing[player].job_next:cancel()
	end
	mcla_music_api.stop_playback(player)
	now_playing[player] = {
		handle = minetest.sound_play({name = def.name, gain = mcla_music_api.get_gain(player)}, {to_player = player:get_player_name()}),
		song = def,
		time_started = minetest.get_gametime()
	}
	for _,v in pairs(mcla_music_api.registered_on_play) do
		if v(player, def) == true then return def end
	end
	if minetest.global_exists("mcl_title") and def.title then
		mcl_title.set(player, "actionbar", {text=def.title, color="#a8dba4"})
	end
	return def
end

function mcla_music_api.play_trackno(player, trackno)
	for _,v in pairs(mcla_music_api.registered_songs) do
		if trackno == v.trackno then
			return mcla_music_api.play_song(player, v)
		end
	end
end

function mcla_music_api.next_song(player)
	return mcla_music_api.play_song(player, mcla_music_api.pick_song(player))
end

minetest.register_on_joinplayer(function(pl, last_login)
	mcla_music_api.next_song(pl)
end)

minetest.register_on_leaveplayer(function(pl)
	if now_playing[pl] and now_playing[pl].job_next then
		now_playing[pl].job_next:cancel()
	end
	now_playing[pl] = nil
end)

minetest.register_globalstep(function()
	for pl, def in pairs(now_playing) do
		if def.time_started and minetest.get_gametime() - def.time_started > def.song.length then
			for _, func in pairs(mcla_music_api.registered_on_song_complete) do
				func(pl, def.song)
			end
			now_playing[pl] = {}
			now_playing[pl].job_next = minetest.after(math.random(time_between_min, time_between_max), function(pl)
				if not pl or not pl:get_pos() then return end
				mcla_music_api.next_song(pl)
			end, pl)
		end
	end
end)

if minetest.global_exists("mcl_worlds") then
	mcl_worlds.register_on_dimension_change(mcla_music_api.next_song)
end

local function get_formspec(player)
	local x, y
	if minetest.global_exists("mcl_inventory") then
		x = 2
		y = 1
	elseif minetest.global_exists("sfinv") then
		x = 0
		y = 1
	else
		return ""
	end
	table.sort(mcla_music_api.registered_songs, function(a, b) return a.trackno < b.trackno end)
	local r = "textlist["..x..","..y..";8,3.5;track;"
	for _, v in ipairs(mcla_music_api.registered_songs) do
		if not v.hidden then
			r = r..(v.title or v.name)..","
		end
	end
	r = r:sub(1,-2) .. ";1;false]"
	if now_playing[player] and now_playing[player].song then
		r = r.."label[0.3,0.3;Now Playing:]"
		r = r.."label[0.3,0.6;"..(now_playing[player].song.title or now_playing[player].song.name).."]"
	end
	return r
end

local function handle_receive_fields(player, fields)
	if fields.track then
		local ev = minetest.explode_textlist_event(fields.track)
		mcla_music_api.play_trackno(player, ev.index)
		if minetest.global_exists("mcl_inventory") then
			minetest.show_formspec(player:get_player_name(),"" , mcl_inventory.build_survival_formspec(player))
		elseif minetest.global_exists("sfinv") then
			minetest.show_formspec(player:get_player_name(),"" , sfinv.make_formspec(player, sfinv.contexts[player:get_player_name()], get_formspec(player)))
		end
	end
end

local function show_inv_tab(player)
	return true
end

if minetest.global_exists("mcl_inventory") then
	minetest.register_on_mods_loaded(function()
		mcl_inventory.register_survival_inventory_tab({
			id = "music",
			description = "Music",
			item_icon = "mcl_jukebox:record_1",
			show_inventory = true,
			build = get_formspec,
			handle = handle_receive_fields,
			access = show_inv_tab,
		})
	end)
end

if minetest.global_exists("sfinv") then
	sfinv.register_page("music", {
		title = "Music",
		get = function(self, player, context)
			return sfinv.make_formspec(player, context, get_formspec(player))
		end,
		is_in_nav = function(self, player, context)	return show_inv_tab(player)	end,
		on_player_receive_fields = function(self, player, context, fields)
			return handle_receive_fields(player, fields)
		end,
	})

end

minetest.register_chatcommand("music", {
	description = "Control background music",
	params = "next|list|play <tracknumber>|volume <level>",
	func = function(pn, pr)
		local pl = minetest.get_player_by_name(pn)
		local num = tonumber(pr)
		if num and num >= 0 and num <= 100 then
			mcla_music_api.set_volume(pl, num)
			return true, "Music volume set to "..tostring(pr)
		elseif pr == "next" then
			local def, _ = mcla_music_api.next_song(pl)
			return true, "Now Playing: "..tostring(def.title or def.name)
		elseif pr == "list" then
			table.sort(mcla_music_api.registered_songs, function(a, b) return a.trackno < b.trackno end)
			local rt = ""
			for _, v in ipairs(mcla_music_api.registered_songs) do
				rt = rt .. tostring(v.trackno)..". "..tostring(v.title or v.name).."\n"
			end
			return true, rt
		elseif pr:split(" ")[1] == "play" then
			local num = tonumber(pr:split(" ")[2])
			local r = mcla_music_api.play_trackno(pl, num)
			if r then
				return true, "Now Playing: "..tostring(r.title or r.name)
			end
			return false, "Track not found"
		end
	end,
})
