# Minimum interval between attempts to play music
music_time_interval_min (Interval between attempts to play music) int 60

# Maximum interval between attempts to play music
music_time_interval_max (Interval between attempts to play music) int 240

#Display info messages in server console
music_display_playback_messages (Display info messages in server console) bool true
