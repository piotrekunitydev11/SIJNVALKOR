#!/bin/sh

prerequisites_not_found=0
if [ "$(which vorbiscomment)" = "" ]; then
	echo "vorbiscomment command not found; install vorbis-tools package"
	prerequisites_not_found=1
fi
if [ "$(which ffmpeg)" = "" ]; then
	echo "ffmpeg command not found; install ffmpeg package"
	prerequisites_not_found=1
fi
if [ $prerequisites_not_found -eq 1 ]; then exit 1; fi

for f in *.ogg; do
	title=$(vorbiscomment $f|grep "title"|sed 's/^title=//')
	artist=$(vorbiscomment $f|grep "^artist="|sed 's/^artist=//')
	album_artist=$(vorbiscomment $f|grep "^album_artist="|sed 's/^album_artist=//')
	echo "mcla_music_api.register_song({";
	echo "\tname = \"$(echo $f| sed 's/.ogg$//')\",";
	if [ "$title" != "" ] || [ "$artist" != "" ]; then
		echo -n "\ttitle = \""
		if [ "$artist" != "" ]; then echo -n "$artist - "; fi
		if [ "$album_artist" != "" ]; then echo -n "$album_artist - "; fi
		echo "$title\",";
	fi
	echo "\tlength = $(ffmpeg -i $f 2>&1 | grep "Duration"| cut -d ' ' -f 4 | sed s/,// | sed 's@\..*@@g' | awk '{ split($1, A, ":"); split(A[3], B, "."); print 3600*A[1] + 60*A[2] + B[1] }')";
	echo "})";
done
