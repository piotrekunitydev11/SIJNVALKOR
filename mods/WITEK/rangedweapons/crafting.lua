----------------------------
----------------------------
if rweapons_gun_crafting == "true" then

minetest.register_craft({
	output = "rangedweapons:aa12",
	recipe = {
		{"", "", ""},
		{"mcl_core:ironblock", "mcl_core:ironblock", "mcl_core:ironblock"},
		{"", "mcl_copper:block", "mcl_core:ironblock"},
	}
})

minetest.register_craft({
	output = "rangedweapons:ak47",
	recipe = {
		{"", "", ""},
		{"group:wood", "group:wood", "moreores:steel_stick"},
		{"moreores:steel_block", "moreores:steel_pipe", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:awp",
	recipe = {
		{"", "", ""},
		{"mcl_nether:netheriteblock", "moreores:steel_stick", "moreores:steel_stick"},
		{"mcl_nether:netherite_ingot", "moreores:steel_pipe", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:benelli",
	recipe = {
		{"", "", ""},
		{"mcl_core:ironblock", "mcl_core:ironblock", "mcl_core:ironblock"},
		{"mcl_core:ironblock", "group:wood", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:beretta",
	recipe = {
		{"", "", ""},
		{"mcl_core:iron_ingot", "mcl_core:iron_ingot", "mcl_core:flint"},
		{"", "", "mcl_nether:netherbrick"},
	}
})

minetest.register_craft({
	output = "rangedweapons:m1991",
	recipe = {
		{"", "", ""},
		{"mcl_nether:netherite_ingot", "mcl_nether:netherite_ingot", ""},
		{"", "mcl_nether:netherite_ingot", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:deagle",
	recipe = {
		{"", "", ""},
		{"mcl_core:ironblock", "mcl_core:ironblock", "mcl_core:ironblock"},
		{"", "", "mcl_core:ironblock"},
	}
})
minetest.register_craft({
	output = "rangedweapons:golden_deagle",
	recipe = {
		{"mineclone:gold_ingot", "mineclone:gold_ingot", "mineclone:gold_ingot"},
		{"mineclone:gold_ingot", "rangedweapons:deagle", "mineclone:gold_ingot"},
		{"mineclone:gold_ingot", "mineclone:gold_ingot", "mineclone:gold_ingot"},
	}
})

minetest.register_craft({
	output = "rangedweapons:python",
	recipe = {
		{"", "", ""},
		{"mcl_core:iron_ingot", "mcl_core:ironblock", "mcl_copper:copper_ingot"},
		{"", "", "mcl_copper:copper_ingot"},
	}
})

minetest.register_craft({
	output = "rangedweapons:g36",
	recipe = {
		{"mcl_core:iron_ingot", "mcl_core:iron_ingot", ""},
		{"mcl_nether:netherite_ingot", "mcl_nether:netherite_ingot", ""},
		{"", "mcl_core:ironblock", "mcl_core:iron_ingot"},
	}
})

minetest.register_craft({
	output = "rangedweapons:m16",
	recipe = {
		{"", "mcl_core:iron_ingot", ""},
		{"moreores:steel_stick", "mcl_core:iron_ingot", "mcl_core:ironblock"},
		{"", "", "mcl_core:ironblock"},
	}
})

minetest.register_craft({
	output = "rangedweapons:m60",
	recipe = {
		{"", "mcl_nether:netheriteblock", ""},
		{"mcl_nether:netheriteblock", "mcl_nether:netheriteblock", "mcl_nether:netherite_ingot"},
		{"", "mcl_nether:netherite_ingot", "mcl_nether:netherite_ingot"},
	}
})

minetest.register_craft({
	output = "rangedweapons:m79",
	recipe = {
		{"mcl_nether:netheriteblock", "", ""},
		{"mcl_nether:netheriteblock", "mcl_nether:netherite_ingot", ""},
		{"mcl_nether:netheriteblock", "mcl_nether:netherite_ingot", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:m200",
	recipe = {
		{"", "", ""},
		{"mcl_core:ironblock", "mcl_core:ironblock", "mcl_core:flint"},
		{"", "", "mcl_core:flint"},
	}
})

minetest.register_craft({
	output = "rangedweapons:glock17",
	recipe = {
		{"", "", ""},
		{"mcl_core:ironblock", "mcl_core:iron_ingot", "moreores:steel_pipe"},
		{"mcl_core:ironblock", "", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:makarov",
	recipe = {
		{"", "", ""},
		{"", "mcl_core:iron_ingot", "mcl_core:iron_ingot"},
		{"", "", "mcl_copper:copper_ingot"},
	}
})

minetest.register_craft({
	output = "rangedweapons:minigun",
	recipe = {
		{"", "", ""},
		{"", "", ""},
		{"", "", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:mp5",
	recipe = {
		{"", "", ""},
		{"mcl_nether:netherite_ingot", "mcl_core:ironblock", "mcl_core:ironblock"},
		{"mcl_core:iron_ingot", "", "mcl_core:iron_ingot"},
	}
})

minetest.register_craft({
	output = "rangedweapons:thompson",
	recipe = {
		{"", "", ""},
		{"group:wood", "moreores:steel_pipe", "moreores:steel_stick"},
		{"mcl_core:goldblock", "moreores:steel_pipe", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:mp40",
	recipe = {
		{"mcl_core:ironblock", "mcl_core:ironblock", "mcl_core:ironblock"},
		{"mcl_core:iron_ingot", "", "mcl_core:iron_ingot"},
		{"mcl_core:iron_ingot", "", ""},
	}
})



minetest.register_craft({
	output = "rangedweapons:remington",
	recipe = {
		{"mcl_core:ironblock", "mcl_core:ironblock", "group:wood"},
		{"", "group:wood", "group:wood"},
	}
})

minetest.register_craft({
	output = "rangedweapons:rpg",
	recipe = {
		{"", "", ""},
		{"", "", ""},
		{"", "", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:rpk",
	recipe = {
		{"mcl_core:iron_ingot", "", ""},
		{"mcl_core:ironblock", "mcl_core:ironblock", "mcl_core:iron_ingot"},
		{"mcl_core:ironblock", "mcl_core:ironblock", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:scar",
	recipe = {
		{"", "", ""},
		{"mcl_core:ironblock", "moreores:steel_block", "moreores:steel_pipe"},
		{"moreores:steel_block", "moreores:steel_block", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:spas12",
	recipe = {
		{"", "", ""},
		{"moreores:steel_block", "mcl_core:ironblock", "mcl_core:ironblock"},
		{"moreores:steel_pipe", "", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:svd",
	recipe = {
		{"", "group:wood", ""},
		{"moreores:steel_block", "moreores:steel_block", "moreores:steel_block"},
		{"", "group:wood", "group:wood"},
	}
})

minetest.register_craft({
	output = "rangedweapons:taurus",
	recipe = {
		{"", "", ""},
		{"mcl_core:iron_ingot", "mcl_core:ironblock", "mcl_core:flint"},
		{"", "", "mcl_core:flint"},
	}
})

minetest.register_craft({
	output = "rangedweapons:tec9",
	recipe = {
		{"", "", ""},
		{"", "", ""},
		{"", "", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:tmp",
	recipe = {
		{"mcl_blackstone:blackstone", "mcl_blackstone:blackstone", "mcl_blackstone:blackstone"},
		{"", "mcl_blackstone:blackstone", "group:wood"},
		{"", "mcl_blackstone:blackstone", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:ump",
	recipe = {
		{"mcl_core:iron_ingot", "mcl_core:iron_ingot", "mcl_core:iron_ingot"},
		{"", "mcl_core:ironblock", "mcl_core:ironblock"},
		{"", "mcl_core:iron_ingot", "mcl_core:iron_ingot"},
	}
})

minetest.register_craft({
	output = "rangedweapons:uzi",
	recipe = {
		{"", "mcl_core:iron_ingot", ""},
		{"moreores:steel_stick", "mcl_core:ironblock", "mcl_core:ironblock"},
		{"", "", "mcl_core:ironblock"},
	}
})

end
----------------------------------
----------------------------------
if rweapons_other_weapon_crafting == "true" then
minetest.register_craft({
	output = "rangedweapons:wooden_shuriken 5",
	recipe = {
		{"", "mineclone:tree", ""},
		{"mineclone:tree", "", "mineclone:tree"},
		{"", "mineclone:tree", ""},
	}
})
minetest.register_craft({
	output = "rangedweapons:stone_shuriken 5",
	recipe = {
		{"", "mineclone:cobble", ""},
		{"mineclone:cobble", "", "mineclone:cobble"},
		{"", "mineclone:cobble", ""},
	}
})
minetest.register_craft({
	output = "rangedweapons:golden_shuriken 5",
	recipe = {
		{"", "mineclone:gold_ingot", ""},
		{"mineclone:gold_ingot", "", "mineclone:gold_ingot"},
		{"", "mineclone:gold_ingot", ""},
	}
})
minetest.register_craft({
	output = "rangedweapons:diamond_shuriken 10",
	recipe = {
		{"", "mineclone:diamond", ""},
		{"mineclone:diamond", "", "mineclone:diamond"},
		{"", "mineclone:diamond", ""},
	}
})

end
------------------------------------
------------------------------------
if rweapons_ammo_crafting == "true" then

minetest.register_craft({
	output = "rangedweapons:9mm 40",
	recipe = {
		{"", "", ""},
		{"tnt:gunpowder", "", ""},
		{"mineclone:copper_ingot", "", ""},
	}
})
minetest.register_craft({
	output = "rangedweapons:45acp 40",
	recipe = {
		{"","mineclone:bronze_ingot", ""},
		{"mineclone:gold_ingot","tnt:gunpowder", "mineclone:gold_ingot"},
		{"","mineclone:tin_ingot", ""},
	}
})
minetest.register_craft({
	output = "rangedweapons:10mm 60",
	recipe = {
		{"", "mineclone:bronze_ingot", ""},
		{"", "tnt:gunpowder", ""},
		{"", "tnt:gunpowder", ""},
	}
})
minetest.register_craft({
	output = "rangedweapons:357 15",
	recipe = {
		{"mineclone:copper_ingot", "", ""},
		{"tnt:gunpowder", "", ""},
		{"mineclone:gold_ingot", "", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:50ae 15",
	recipe = {
		{"mineclone:bronze_ingot", "mineclone:coal_lump", "mineclone:bronze_ingot"},
		{"mineclone:gold_ingot", "tnt:gunpowder", "mineclone:gold_ingot"},
		{"mineclone:gold_ingot", "tnt:gunpowder", "mineclone:gold_ingot"},
	}
})

minetest.register_craft({
	output = "rangedweapons:44 15",
	recipe = {
		{"mineclone:bronze_ingot", "mineclone:coal_lump", ""},
		{"tnt:gunpowder", "", ""},
		{"mineclone:gold_ingot", "", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:762mm 50",
	recipe = {
		{"mineclone:bronze_ingot", "tnt:gunpowder", "mineclone:bronze_ingot"},
		{"mineclone:gold_ingot", "tnt:gunpowder", "mineclone:gold_ingot"},
		{"mineclone:gold_ingot", "tnt:gunpowder", "mineclone:gold_ingot"},
	}
})

minetest.register_craft({
	output = "rangedweapons:408cheytac 10",
	recipe = {
		{"mineclone:bronze_ingot", "tnt:gunpowder", "mineclone:bronze_ingot"},
		{"mineclone:gold_ingot", "mineclone:gold_ingot", "mineclone:gold_ingot"},
		{"mineclone:gold_ingot", "tnt:gunpowder", "mineclone:gold_ingot"},
	}
})

minetest.register_craft({
	output = "rangedweapons:556mm 90",
	recipe = {
		{"", "mineclone:gold_ingot", ""},
		{"mineclone:gold_ingot", "tnt:gunpowder", "mineclone:gold_ingot"},
		{"mineclone:gold_ingot", "tnt:gunpowder", "mineclone:gold_ingot"},
	}
})
minetest.register_craft({
	output = "rangedweapons:shell 12",
	recipe = {
		{"mineclone:bronze_ingot", "", "mineclone:bronze_ingot"},
		{"mineclone:bronze_ingot", "tnt:gunpowder", "mineclone:bronze_ingot"},
		{"mineclone:gold_ingot", "tnt:gunpowder", "mineclone:gold_ingot"},
	}
})
minetest.register_craft({
	output = "rangedweapons:308winchester 15",
	recipe = {
		{"", "", ""},
		{"mineclone:bronze_ingot", "tnt:gunpowder", "mineclone:bronze_ingot"},
		{"mineclone:gold_ingot", "tnt:gunpowder", "mineclone:gold_ingot"},
	}
})
minetest.register_craft({
	output = "rangedweapons:40mm 5",
	recipe = {
		{"", "mineclone:gold_ingot", ""},
		{"", "tnt:gunpowder", ""},
		{"tnt:gunpowder", "mineclone:bronze_ingot", "tnt:gunpowder"},
	}
})
minetest.register_craft({
	output = "rangedweapons:rocket 1",
	recipe = {
		{"", "", "rangedweapons:40mm"},
		{"", "tnt:gunpowder", ""},
		{"", "", ""},
	}
})

end
-------------------------------------
-------------------------------------
if rweapons_item_crafting == "true" then

minetest.register_craft({
	output = "rangedweapons:generator",
	recipe = {
{"mineclone:gold_ingot", "mineclone:gold_ingot", "mineclone:gold_ingot"},
		{"", "rangedweapons:gun_power_core", ""},
		{"", "", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:ultra_gunsteel_ingot",
	recipe = {
		{"", "", ""},
		{"mineclone:gold_ingot", "", "mineclone:gold_ingot"},
		{"", "", ""},
	}
})

minetest.register_craft({
	output = "rangedweapons:gun_power_core",
	recipe = {
		{"", "mineclone:goldblock", ""},
		{"", "", ""},
		{"", "mineclone:goldblock", ""},
	}
})

end