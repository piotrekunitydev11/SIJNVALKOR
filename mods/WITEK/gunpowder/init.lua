local S = minetest.get_translator(minetest.get_current_modname())

minetest.register_node("gunpowder:gunpowder_dust", {
	description = S("Loose Gunpowder Dust"),
	drawtype = "raillike",
	paramtype = "light",
	is_ground_content = false,
	sunlight_propagates = true,
	walkable = false,
	tiles = {
		"tnt_gunpowder_straight.png",
		"tnt_gunpowder_curved.png",
		"tnt_gunpowder_t_junction.png",
		"tnt_gunpowder_crossing.png"
	},
	inventory_image = "gunpowder_inv.png",
	wield_image = "gunpowder_inv.png",
	selection_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, -1/2+1/16, 1/2},
	},
	groups = {dig_immediate = 2, attached_node = 1, flammable = 5,
		connect_to_raillike = minetest.raillike_group("gunpowder")},
	sounds = mcl_sounds.node_sound_leaves_defaults(),

	on_punch = function(pos, node, puncher)
		if puncher:get_wielded_item():get_name() == "mcl_torches:torch" or puncher:get_wielded_item():get_name() == "mcl_fire:flint_and_steel" then
			minetest.set_node(pos, {name = "gunpowder:gunpowder_burning"})
		end
	end,
	on_blast = function(pos, intensity)
		minetest.set_node(pos, {name = "gunpowder:gunpowder_burning"})
	end,
	on_burn = function(pos)
		minetest.set_node(pos, {name = "gunpowder:gunpowder_burning"})
	end,
	on_ignite = function(pos, igniter)
		minetest.set_node(pos, {name = "gunpowder:gunpowder_burning"})
	end,
})

minetest.register_node("gunpowder:gunpowder_burning", {
	drawtype = "raillike",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	light_source = 5,
	tiles = {{
		name = "tnt_gunpowder_burning_straight_animated.png",
		animation = {
			type = "vertical_frames",
			aspect_w = 16,
			aspect_h = 16,
			length = 1,
		}
	},
	{
		name = "tnt_gunpowder_burning_curved_animated.png",
		animation = {
			type = "vertical_frames",
			aspect_w = 16,
			aspect_h = 16,
			length = 1,
		}
	},
	{
		name = "tnt_gunpowder_burning_t_junction_animated.png",
		animation = {
			type = "vertical_frames",
			aspect_w = 16,
			aspect_h = 16,
			length = 1,
		}
	},
	{
		name = "tnt_gunpowder_burning_crossing_animated.png",
		animation = {
			type = "vertical_frames",
			aspect_w = 16,
			aspect_h = 16,
			length = 1,
		}
	}},
	selection_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, -1/2+1/16, 1/2},
	},
	drop = "",
	groups = {
		dig_immediate = 2,
		attached_node = 1,
		connect_to_raillike = minetest.raillike_group("gunpowder"),
		not_in_creative_inventory = 1
	},
	sounds = mcl_sounds.node_sound_leaves_defaults(),
	on_timer = function(pos, elapsed)
		for dx = -1, 1 do
		for dz = -1, 1 do
			if math.abs(dx) + math.abs(dz) == 1 then
				for dy = -1, 1 do
					local check_pos = {x = pos.x + dx, y = pos.y + dy, z = pos.z + dz}
					local node = minetest.get_node(check_pos)
					if node.name == "gunpowder:gunpowder_dust" then
						-- Ignite the gunpowder dust
						minetest.set_node(check_pos, {name = "gunpowder:gunpowder_burning"})
					elseif node.name == "mcl_tnt:tnt" then
						-- Only ignite TNT if the block is TNT
						tnt.ignite(check_pos)
					elseif node.name == "c4:minitnt" then
					-- Ignite MINITNT
					minetest.remove_node(check_pos)
					nuke.spawn_tnt(check_pos, "c4:minitnt")
					elseif node.name == "c4:packedtnt" then
					-- Ignite Packed C4
					minetest.remove_node(check_pos)
					nuke.spawn_tnt(check_pos, "c4:packedtnt")
					end
				end
			end
		end
		end
		minetest.remove_node(pos)
	end,
	-- unaffected by explosions
	on_blast = function() end,
	on_construct = function(pos)
		minetest.sound_play("tnt_gunpowder_burning", {pos = pos,
			gain = 1.0}, true)
		minetest.get_node_timer(pos):start(1)
	end,
})

minetest.register_craft({
	output = "gunpowder:gunpowder_dust 6",
	type = "shapeless",
	recipe = {"mcl_core:coal_lump", "mcl_core:gravel"}
})
